# VBA速记

[TOC]

### 数据类型

1. 数字类型
   1. Byte
   2. Integer
   3. Long
   4. Single
   5. Double
   6. Currency
   7. Decimal
2. 非数字类型
   1. String
   2. Boolean
   3. Date
   4. Object
3. 通用类型
   1. Variant

### 程序结构

#### 选择结构

##### If 语句

```vbscript
If con Then
    pass
```

```vbscript
If con Then
    pass
Else
    pass
End If
```

```vbscript
If con1 Then
    pass
ElseIf con2 Then
    pass
Else
    pass
End If
```

#### Select 语句

```vb
Select Case key
    Case 1
        pass
    Case 2
        pass
    Case Else
        pass
End Select
```

#### 循环结构

##### For 循环

```vb
For i = start To end Step w
    pass
Next i
```

```vb
For Each e In elements
    pass
Next e
```

##### Do While 循环

```vb
Do While con
    pass
Loop
```

```vb
Do
    pass
Loop While con
```

##### Do Until 循环

```vb
Do Until con
    pass
Loop
```

```vb
Do
    pass
Loop Until con
```

`Exit For`语句跳出For循环

`Exit Do`语句跳出Do循环

#### With 结构

```vb
With Worksheets("Sheet1")
    .Name = "..."
    ....
End With
```

链式访问VBA对象的属性和方法

#### Goto 语句

直接跳转, 运行的并不多
