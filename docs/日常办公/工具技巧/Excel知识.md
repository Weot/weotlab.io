# Excel 基础知识

## 一. Excel 中的数据类型

1. 数值
   
   数值类型为Excel中最常见的类型, 日常遇到的各种格式的数字, 比如货币形式, 还是会计形式的数字, 实质上都为数值类型
   
   日期类型看起来比较特别, 实质也是数值类型的一种, 将日期值粘贴到另一个单元格的时候, 可以看到是变成一个数字, 日期类型的单元格中存储的数字实际上是距离1990年1月1日的一个天数

2. 文本
   
   除数值类型外, Excel中文本类型遇到的也比较多, 任何一个单元格中如果出现有除数字外的字符会自动转换为文本.
   
   对于纯数字, 例如身份证号, 银行卡号等, 为了防止自动转换位长整型数字而导致尾数被四舍五入而失去真实值, 可以在输入之前加入单撇号"'", 强制指定单元格为文本格式.
   
   单撇号只是作为前导符号, 不会改变原始值, 查找

3. 逻辑值
   
   逻辑值有TRUE和FALSE, 即真和假, 通常判断某种情况的真假产生的, 比如A1是否与A2相等

4. 错误值
   
   - #DIV/0!: 表示公式中产生除数或者分母为零的错误
   
   - #N/A: 表示引用的单元格中没有可以使用的数值
   
   - #NAME?: 表示公式中含有不能识别的名字或者字符，此时就要检查公式中引用的单元格名称是否输入了不正确的字符
   
   - #REF!: 表示引用中有无效的单元格，移动，复制和删除公式中的引用区域时，应当注意是否破坏了公式中的单元格引用，检查公式中是否有无效的单元格引用
   
   - #VALUE!: 表示在需要数值或者逻辑值的地方输入了文本，需检查公式或者函数的数值和参数
   
   - #NUM!: 表示公式中某个函数的参数不对，需要检查每个函数的参数是否正确

5. 数组
   
   包含一组元素的一种排列, 元素可以是前面的数值, 文本或者逻辑值.
   
   数组属于比较高阶的用法, 通常用不到, 可以简单了解

## 二. Excel 中常用的函数

### 函数是什么?

函数是通过对输入值进行计算操作, 获得我们想要的结果

一般的函数调用包含函数名和参数, 比如round(3.14159, 2), round就是函数名, ()括号代表对函数的调用, 3.14159和2为round函数接收的两个参数, 在单元格中输入"=round(3.14159, 2)", 我们可以得到结果3.14

还有一些函数, 是不需要参数作为输入值, 可以直接返回结果的, 例如now(), today()等函数可以直接返回当时的时间, 当天的日期

### 常用函数

round()函数

对目标数字按指定位数进行四舍五入, 第一个参数为要四舍五入的数字, 第二个参数为要保留的小数位数, 比如要保留2位小数, 第二个参数就为2

第二个参数也可以是0或负数, 0表示四舍五入到各位, -1表示到十位, 以此类推

相对引用与绝对引用

相对引用的情况下, 我们粘贴公式, Excel会自动根据被粘贴的单元格的位置, 自动的改变

但是有些情况下, 我们想要某个引用的单元格或者行或者列保持不变, 就需要用到绝对引用

绝对引用需要再行列表示前加上\$符号, 例如`$A$1`

例子: 横向为每个项目的分配比例, 首列为分配总额

vlookup()

left()

right()

mid()

text()

## 三. 数据透视表
