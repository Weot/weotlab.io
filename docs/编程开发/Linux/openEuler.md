---
title: 虚拟机下openEuler的安装与一些常见服务部署
date: 2023-05-27 21:15
updated: 
categories:
- 编程开发
- Linux
tags: [openEuler, uWsgi, Nginx, ssh, SeLinux]
typora-root-url: ..
root-rul: ..
---
# 虚拟机下openEuler的安装与一些常见服务部署

## 网络问题

NAT模式下, 虚拟网卡VMnet8相当于一个虚拟的路由器, 分出相应的子网

某些情网络配置并不正确, 需要将网关等配置与虚拟网卡保持一致

修改对应网卡配置文件`sudo vim /etc/sysconfig/network-scripts/ifcfg-ens33`:

```ini
TYPE=Ethernet
BOOTPROTO=static
NAME=ens33
DEVICE=ens33
ONBOOT=yes
IPADDR=192.168.101.130
NETMASK=255.255.255.0
GATEWAY=192.168.101.2
```
![虚拟网卡VMnet8信息](./assets/images/openEuler/虚拟网卡VMnet8信息.png)

`IPADDR`: 服务器的地址, 选择一个合适地址, 保证和虚拟网卡在一个网段就好
`GATEWAY`: 网关, 一般为虚拟网卡同一网卡的第2个地址, 例子中为`192.168.101.2`

安装好的虚拟机, 虚拟网卡的信息应该类似图中所示, 如果缺少部分, 需要在win下配置对应的信息

## uWsgi配置与nginx代理

`uWsgi` 安装过程没有太多坑, `pip install uwsgi`一般会遇到一些问题, 编译错误的根据提示下载`gcc`或者`python-dev`之类的就可以解决


安装后可以简单的配置, 先来一个简单的配置文件

```ini
[uwsgi]
#uwsgi启动时，所使用的地址和端口（这个是http协议的）
#http=0.0.0.0:5000
#指向网站目录
chdir=/home/miao/myexam/
#python 启动程序文件
wsgi-file=wsgi.py
#python 程序内用以启动的application 变量名
callable=app
#处理器数
processes=1
#线程数
threads=2
# uwsig pid 号
pidfile=/home/miao/uwsgi/uwsgi.pid

# 重启的时候使用的 pid 号
touch-reload=/home/miao/uwsgi/uwsgi.pid

# unix socket和TCP socket配置, 在nginx可以使用这两个socket
socket=/home/miao/uwsgi/uwsgi.sock
socket=0.0.0.0:5000
chomod-socket=666
```

运行`/home/miao/.venv/myflask/bin/uwsgi --ini uwsgi.ini`, 看是否可以运行

每次手动运行显然不够优雅, 配置好这个再配置对应的开机自启

`Nginx`中相应配置, 文件位置一般在`/etc/nginx/nginx.conf`:

第一种模式, 单机, 直接再`server`配置下增加`location`的配置

```nginx
...
...
server {
    listen       80;
    listen       [::]:80;
    server_name  192.168.101.130;
    root         /usr/share/nginx/html;

    location / {
        include uwsgi_params;
        uwsgi_pass unix:///home/miao/uwsig/uwsgi.sock;
        # uwsgi_pass 127.0.0.1:5000;
    }
}
...
...
```
`uwsgi_pass` 指向之前在`uwsgi.ini`中配置的`unix socket`或者`TCP socket`

第二种模式, 集群, 在`server`外添加`upstream`

```nginx
http {

    ...
    ...
    upstream uwsgicluster {
        server unix:///home/miao/uwsig/uwsgi.sock;
        server 127.0.0.1:5000;
    }

    server {
        listen       80;
        listen       [::]:80;
        server_name  192.168.101.130;
        root         /usr/share/nginx/html;

        location / {
            include uwsgi_params;
            uwsgi_pass uwsgicluster;
        }
    ...
    }
}
```

这里将在`upstream`中配置集群, `server`指向对应的`socket`, 在`location`中将`uwsgi_pass`指向集群`uwsgicluster`

这些配置后可能遇到的问题:
1. `nginx`启动后, 无法正常访问页面

关闭防火墙试一试
```shell
sudo systemctl stop firewalld
sudo systemctl disable firewalld
```

2. `nginx`可以启动正常运行, 但`uWsgi`启动后, 无法被`nginx`代理

可能是权限问题, `nginx`无法读取对应的`unix socket`, 单机可以使用, 速度应该会更好一点

注意在`uwsgi.ini`添加对应配置, 升级权限
```ini
chomod-socket=666
```

## `uWsgi`使用`systemd`方式自启动

添加配置文件`/etc/systemd/system/uwsgi.service`

```systemd
[Unit]
Description=uwsgi
After=syslog.target

[Service]
Type=notify
ExecStart=/home/miao/.venv/myflask/bin/uwsgi --ini /home/miao/uwsgi/uwsgi.ini
ExecStop=/home/miao/.venv/myflask/bin/uwsgi --stop /home/miao/uwsgi/uwsgi.pid
ExecReload=/home/miao/.venv/myflask/bin/uwsgi --reload /home/miao/uwsgi/uwsgi.pid
User=miao
Group=miao
Restart=on-failure
KillSignal=SIGQUIT
StandardError=syslog
NotifyAccess=all
[Install]
WantedBy=multi-user.target
```

配置好后, 使用下面命令重新加载

```shell
sudo systemctl daemon-reload
```

启动`uwsgi`

```shell
sudo systemctl start uwsgi # 启动
sudo systemctl enable uwsgi # 开机自启
sudo systemctl stop uwsgi # 关闭
```


如果出现错误, 可以查看是系统日志文件`/var/log/message`, 如果出现`Permission denied`的错误, 大概率是`SElinux`在阻止的问题

关闭`SElinux`试一试, `vim /etc/selinux/config`

```ini
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=disabled
# SELINUXTYPE= can take one of these three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted
```

设置`SELINUX=disabled`

重新启动

## ssh远程登陆use ssh-key

1. 本地生成密钥文件

    ```shell
    ssh-keygen -t rsa
    ```

    这一步会在`~/.ssh/`目录下生成公钥和私钥文件

2. 将`~/.ssh/`下的公钥文件`id_rsa.pub`复制到远程服务器


    ```cmd
    ssh-copy-id -i ~/.ssh/id_rsa.pub miao@192.168.101.130
    ```

    成功后, 可以在尝试登录, 现在就不需要输入密码了

